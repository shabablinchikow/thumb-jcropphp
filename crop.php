<?php

$src = 'uploads/'.$_GET['upload'];
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$targ_w = 1280; //set output image size
	$targ_h = 750;
	$jpeg_quality = 100;

	$src = $_POST['upload'];

	$exploded = explode('.',$src);
  	$ext = $exploded[count($exploded) - 1];

    if (preg_match('/jpg|jpeg/i',$ext))
        $img_r=imagecreatefromjpeg($src);
    else if (preg_match('/png/i',$ext))
        $img_r=imagecreatefrompng($src);
    else if (preg_match('/gif/i',$ext))
        $img_r=imagecreatefromgif($src);
    else if (preg_match('/bmp/i',$ext))
        $img_r=imagecreatefrombmp($src);

	$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
	$water_w = imagecreatefrompng ('pic/wooz_template_white.png'); //path to templates 
	$water_b = imagecreatefrompng ('pic/wooz_template_black.png');

	imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
	$targ_w,$targ_h,$_POST['w'],$_POST['h']);
	imagesavealpha($dst_r, true);
	imagealphablending($dst_r, true);
	if ($_POST['color']=='2')
		{
			imagecopy($dst_r, $water_b, 0, 0, 0, 0, $targ_w, $targ_h);
			$color=imagecolorallocatealpha($dst_r, 40, 40, 40, 40);
		}
	else
		{
			imagecopy($dst_r, $water_w, 0, 0, 0, 0, $targ_w, $targ_h);
			$color=imagecolorallocatealpha($dst_r, 255, 255, 255, 0);
		}

	$color2=imagecolorallocatealpha($dst_r, 255, 255, 255, 0);
	imagesavealpha($dst_r, true);
	imagealphablending($dst_r, true);
	
	imagettftext($dst_r, 26, 0, 88, 434, $color, 'font/BebasNeueBold.ttf', strtoupper($_POST['type'])); //font size and position must be tested and set manually
	imagettftext($dst_r, $_POST['title_size'], 0, 41, 585, $color2, 'font/BebasNeueBold.ttf', strtoupper($_POST['title']));
	imagettftext($dst_r, $_POST['subtitle_size'], 0, 41, 660, $color2, 'font/BebasNeueBook.ttf', $_POST['subtitle']);

	$dt = date(time());
	header('Content-type: image/jpeg');
	header('Content-Disposition: inline; filename="wooz'. $dt .'.jpg"');
	header("Content-Type: application/force-download");
	imagejpeg($dst_r,null,$jpeg_quality);

	exit;
}

// If not a POST request, display page below:

?><!DOCTYPE html>
<html lang="ru">
<head>
  <title>Thumb-JcropPHP</title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.Jcrop.js"></script>
  <link rel="stylesheet" href="css/main.css" type="text/css" />
  <link rel="stylesheet" href="css/demos.css" type="text/css" />
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />

<script type="text/javascript">

  jQuery(function($){

		var jcrop_api,
			boundx,
			boundy,
			hSize

		$preview = $('#preview-pane'),
    $pcnt = $('#preview-pane .preview-container'),
    $pimg = $('#preview-pane .preview-container img'),

    xsize = $pcnt.width(),
    ysize = $pcnt.height();


		$("#headerQuery").keyup(function() {
        $("#headerText").html($(this).val());
		});
		$("#subQuery").keyup(function() {
        $("#subheaderText").html($(this).val());
		});
		$("#typeQuery").keyup(function() {
        $("#typeText").html($(this).val());
		});
		$("#headerSize").keyup(function() {
				hSize = parseInt($("#headerSize").val())*28/90;
        $("#headerText").css('font-size', hSize);
		});
		$("#subSize").keyup(function() {
				hSize = parseInt($("#subSize").val())*19.8/64;
        $("#subheaderText").css('font-size', hSize);
		});
		$('input[type=radio][name=color]').change(function() {
				if ((parseInt($('input[type=radio][name=color]:checked').val())) == 1)
				{	$("#typeText").css('color', 'white');
					$("#prvImg").attr('src','pic/wooz_template_white.png');}
				if ((parseInt($('input[type=radio][name=color]:checked').val())) == 2)
				{	$("#typeText").css('color', 'black');
					$("#prvImg").attr('src','pic/wooz_template_black.png');}
    });

		$('#cropbox').Jcrop({
      aspectRatio: 1280/720,
			boxWidth: 555,
			boxHeight: 333,
			onChange: updatePreview,
			onSelect: updateCoords
    },
		function(){
	      // Use the API to get the real image size
	      var bounds = this.getBounds();
	      boundx = bounds[0];
	      boundy = bounds[1];
	      // Store the API in the jcrop_api variable
	      jcrop_api = this;

	      // Move the preview into the jcrop container for css positioning
	      $preview.appendTo(jcrop_api.ui.holder);
	    });

	function updatePreview(c)
    {
      if (parseInt(c.w) > 0)
      {
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * c.x) + 'px',
          marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
      }
    };

	function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Select the area, you want to crop, dumbass');
    return false;
  };
});
</script>
<script>

function line()
{
    var canvas = document.getElementById('line');
    var obCanvas = canvas.getContext('2d');
		var canvas2 = document.getElementById('line2');
    var obCanvas2 = canvas2.getContext('2d');

    obCanvas.beginPath();
    obCanvas.lineWidth = 2;
    obCanvas.strokeStyle = 'red';
    obCanvas.moveTo(10, 0);
    obCanvas.lineTo(10, 500);
    obCanvas.stroke();

		obCanvas2.beginPath();
    obCanvas2.lineWidth = 2;
    obCanvas2.strokeStyle = 'red';
    obCanvas2.moveTo(290, 0);
    obCanvas2.lineTo(290, 500);
    obCanvas2.stroke();
}

</script>
<style type="text/css">
  #target {
    background-color: #ccc;
    width: 500px;
    height: 330px;
    font-size: 24px;
    display: block;
  }

	.jcrop-holder #preview-pane {
	  display: block;
	  position: absolute;
	  z-index: 2000;
	  right: -330px;
	  padding: 6px;
	  border: 1px rgba(0,0,0,.4) solid;
	  background-color: white;

	  -webkit-border-radius: 6px;
	  -moz-border-radius: 6px;
	  border-radius: 6px;

	  -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
	  -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
	  box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
	}

	/* The Javascript code will set the aspect ratio of the crop
	   area based on the size of the thumbnail preview,
	   specified here */
	#preview-pane .preview-container {
	  width: 300px;
	  height: 170px;
	  overflow: hidden;
	}

	@font-face {
	    font-family: "BebasNeueBold";
	    src: url(http://serv.nenarkosha.ru/wooz/font/BebasNeueBold.ttf) format("truetype");
	}
	@font-face {
			font-family: "BebasNeueBook";
			src: url(http://serv.nenarkosha.ru/wooz/font/BebasNeueBook.ttf) format("truetype");
	}
</style>

</head>
<body onLoad="line()">

<div class="container">
<div class="row">
<div class="span12">
<div class="jc-demo-box">

<div class="page-header">
<ul class="breadcrumb first">
  <li><a href="http://git.nenarkosha.ru/nenarkosha/thumb-jcropphp">Thumb-JcropPHP</a> <span class="divider">/</span></li>
  <li><a href="http://nenarkosha.ru">NEnarkosha</a> <span class="divider">/</span></li>  
</ul>
<h1>Thing to do thumb pics for social networks with text on it</h1>
</div>


		<img src="<?php echo $src; ?>" id="cropbox" />

		<div id="preview-pane">
    <div class="preview-container">
      <img src="<?php echo $src; ?>" class="jcrop-preview" alt="Preview" />
    </div>

		<img id="prvImg" src="pic/wooz_template_black.png" alt="Preview" style="position:absolute; top:10px;  width: 300px; height: 170px;" />
		<canvas id="line" style="position:absolute; top:15px"></canvas>
		<canvas id="line2" style="position:absolute; top:15px"></canvas>
		<p id="typeText" style="position:absolute; top:96px; left:26px; font-family:BebasNeueBold; color:black; font-size:8.9px;"></p>
		<p id="headerText" style="position:absolute; top:125px; left:16px; font-family:BebasNeueBold; color:white; font-size:28px;"></p>
		<p id="subheaderText" style="position:absolute; top:144px; left:16px; font-family:BebasNeueBook; color:white; font-size:19.2px;"></p>

  </div>


		<form action="crop.php" method="post" onsubmit="return checkCoords();">
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			<input type="hidden" name="upload" value="<?php echo $src; ?>"/>
			News type: <input type="text" id="typeQuery" name="type" /> <br>
			Title: <input type="text" id="headerQuery" name="title" /> <br>
			Title size(px): <input type="text" id="headerSize" name="title_size" value="90"/> <br>
			Subtitle: <input type="text" id="subQuery" name="subtitle" /> <br>
			Subtitle size(px): <input type="text" id="subSize" name="subtitle_size" value="64"/> <br>
			<input type="radio" name="color" value="1" /> White template
			<input type="radio" name="color" value="2" checked="checked"/> Dark template <br> <br>
			<input type="submit" value="JUST DO IT" class="btn btn-large btn-inverse" />
		</form>

		<p>
			Thing to do thumb pics for social networks with text on it<br>
			By <a href="http://nenarkosha.ru">NEnarkosha</a>, based on <a href="http://deepliquid.com/content/Jcrop.html">Jcrop</a>
		</p>


	</div>
	</div>
	</div>
	</div>
	</body>

</html>
